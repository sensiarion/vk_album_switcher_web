import json
import re
import queue
from urllib.request import urlretrieve

import requests
import flask


def get_url(vk_api_client_id, scope):
    """
    :param vk_api_client_id: <int> id of vk standalone app
    :param scope: <str> query of access to user's data
    :param chat_id: <int> telegram chat_id
    :return: <str> prepeared for give token query
    """
    url = "https://oauth.vk.com/authorize?client_id=%d&display=page&redirect_uri=blank.html" \
          "&scope=%s&response_type=token&v=5.80" % (
              vk_api_client_id, scope)
    return url


def url_validate(url):
    """
    :param url: <str> url to vk token page
    :return: list of url's get params (token,user_id,state only)
    """
    if 'https://oauth.vk.com/blank.html#access_token=' in url:
        cuttted = url[url.find('token=') + 6:]
        token = cuttted[:cuttted.find('&')]
        temp = cuttted[cuttted.find('user_id=') + 8:]
        vk_user_id = temp[:temp.find('&')]
        state = cuttted[cuttted.find('state=') + 6:]

        return token

    return False


def make_request_url(metod_name, params, version='5.80'):
    # делает запрос к vk api с заданными параметрами
    def_url = "https://api.vk.com/method/%s?" % metod_name
    for param in params.keys():
        def_url += param + '=' + str(params.get(param)) + '&'
    def_url = def_url[:-1]

    return def_url + '&v=' + str(version)


def get_album_ident(url):
    # Получает id пользователя и альбома
    temp = url.split('https://vk.com/album')
    return temp[1].split("_")


def get_photos(album_url, token):
    # Возвращает адреса всех фотографии
    owner_id, album_id = get_album_ident(album_url)
    get_album_ids = make_request_url('photos.get', {'access_token': token,
                                                    'owner_id': owner_id,
                                                    'album_id': album_id})
    response = requests.get(get_album_ids)
    photo_list = queue.Queue()
    if response.status_code == 200:
        try:
            json = response.json()['response']
        except KeyError:
            print('Не удалось получить пользователя, скорее всего ошибка в самом запросе')
            print(make_request_url('photos.get', {'access_token': token,
                                                  'owner_id': owner_id,
                                                  'album_id': album_id}))
            print(response.status_code)
            print(response.content)
            raise KeyError
        for item in json['items']:
            last_q = len(item['sizes'])
            photo_list.put({
                'src': item['sizes'][last_q - 1]['url'],
                'text': item['text']
            })
    return photo_list


def get_album_upload_server(album_url, token):
    owner_id, album_id = get_album_ident(album_url)
    if int(owner_id) > 0:
        url = make_request_url('photos.getUploadServer', {'access_token': token,
                                                          'album_id': album_id})
    else:
        url = make_request_url('photos.getUploadServer', {'access_token': token,
                                                          'album_id': album_id,
                                                          'group_id': owner_id})
    r = requests.get(url)
    if r.status_code == 200:
        try:
            json = r.json()['response']
        except KeyError:
            print('Ошибка в составлении запроса')
            print(r.status_code)
            print(r.content)
            raise KeyError
    else:
        print('Неудачный запрос к серверу, с соединением всё в порядке ?')
        print(r.status_code)
        print(r.content)
        raise ConnectionError

    return json['upload_url']


def load_to_album(token, album_url, photos_urls):
    counter = 0
    owner_id, album_id = get_album_ident(album_url)
    filenames = queue.Queue()
    descriptions = queue.Queue()
    while not photos_urls.empty():
        photo_url = photos_urls.get()
        descriptions.put(photo_url['text'])
        filenames.put(str(counter) + '.' + photo_url['src'][-3:])
        urlretrieve(photo_url['src'], str(counter) + '.' + photo_url['src'][-3:])
        counter += 1
        if counter == 5:
            server = get_album_upload_server(album_url, token)
            data = []
            for i in range(5):
                filename = filenames.get()
                print(filename)
                data.append(('file' + str(i), (open(filename, 'rb'))))
            r = requests.post(server, files=data)
            if r.status_code != 200:
                print('Не удалось загрузить фото')
                print(r.status_code)
                print(r.content)
                raise ConnectionError
            json = r.json()
            print(json)
            if int(owner_id) < 0:
                commit_url = make_request_url('photos.save', {'access_token': token,
                                                              'album_id': album_id,
                                                              'group_id': owner_id,
                                                              'server': json['server'],
                                                              'photos_list': json['photos_list'],
                                                              'hash': json['hash']})
            else:
                commit_url = make_request_url('photos.save', {'access_token': token,
                                                              'album_id': album_id,
                                                              'server': json['server'],
                                                              'photos_list': json['photos_list'],
                                                              'hash': json['hash']})
            commit_r = requests.get(commit_url)
            if commit_r.status_code != 200:
                print('Ошибка при подтверждении загрузки фото')
                print(commit_r.status_code)
                print(commit_r.content)
                raise ConnectionError
            print('--------------')
            try:
                commited_photos = commit_r.json()['response']
            except KeyError:
                print('Возникла ошибка при извлечении информации о загруженных фото')
                raise KeyError
            print(commited_photos)
            for photo in commited_photos:
                edit_url = make_request_url('photos.edit', {'access_token': token,
                                                            'owner_id': owner_id,
                                                            'photo_id': photo['id'],
                                                            'caption': descriptions.get()})
                edit_r = requests.get(edit_url)
                if edit_r.status_code != 200:
                    print('Что-то пошло не так при изменении описания фото')
                    print(edit_r.status_code)
                    print(edit_r.content)
                    raise ConnectionError
            counter = 0


app = flask.Flask(__name__)


@app.route('/album_changer', methods=['POST', 'GET'])
def changer():
    return flask.render_template('index.html', token_url=get_url(6265798, 'groups,photos,offline'))


@app.route('/start_copying', methods=['POST'])
def copy():
    token = flask.request.form['token']
    from_url = flask.request.form['from_url']
    to_url = flask.request.form['to_url']
    print(from_url)
    print(to_url)
    print(token)
    photos = get_photos(from_url, token)
    load_to_album(token, to_url, photos)

    return flask.jsonify({'status': 'Success',
                          'code': 200})


if __name__ == '__main__':
    pass
    # url = get_url(6265798, 'groups,photos,offline')
    # token = url_validate(url)

    # user_album = "https://vk.com/album89548778_179214333"
    # group_album = "https://vk.com/album-117584600_255344189"
    # print(get_photos(user_album, token))

    app.run('0.0.0.0', '80', True)
