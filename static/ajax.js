$(document).ready(function () {
    $("#btn").click(
        function () {
            $.ajax({
        type: 'POST', //метод отправки
        url: '/start_copying', //url страницы (action_ajax_form.php)

        data: $('#ajax_form').serialize(),

        success: function (response) { //Данные отправлены успешно
            result = $.parseJSON(response);
            $('#result_form').html('Статус: ' + result.status);
        },
        error: function (response) { // Данные не отправлены
            $('#result_form').html('Ошибка. Данные не отправлены.');
        }
    });
        }
    );
});